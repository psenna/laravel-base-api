<?php


namespace Psenna\LaravelApi\Controller;


use Dingo\Api\Http\Request;
use Dingo\Api\Routing\Helpers;
use Laravel\Lumen\Routing\Controller as LumenBaseController;
use Illuminate\Support\Str;
use Spatie\QueryBuilder\QueryBuilder;

class BaseController extends LumenBaseController
{
    use Helpers;

    protected $model;
    protected $transformer;
    protected $className;
    protected $apiDef;

    protected $defaultPageCount = 10;
    protected $defaultPagelimit = 200;

    /**
     * Mostra lista de recursos.
     *
     * @return \Dingo\Api\Http\Response
     */
    public function index(Request $request) {
        $pageCount = $this->defaultPageCount;

        if ($request->get('limit') && $request->get('limit') != 0) {
            $pageCount = min($request->get('limit'), $this->defaultPagelimit);
        }

        $query = QueryBuilder::for($this->model);

        if($this->apiDef) {
            // Adiciona filtros
            if(is_array($this->apiDef::getAllowedFilters()))
            {
                $query->allowedFilters($this->apiDef::getAllowedFilters());
            }

            if(is_array($this->apiDef::getAllowedIncludes()) && !$this->hasCount())
            {
                $query->allowedIncludes($this->apiDef::getAllowedIncludes());
            }

            if(is_array($this->apiDef::getAllowedFields()) && !$this->hasCount())
            {
                $query->allowedFields($this->apiDef::getAllowedFields());
            }

            if(is_array($this->apiDef::getAllowedSort()) && !$this->hasCount())
            {
                $query->allowedSorts($this->apiDef::getAllowedSort());
                $this->addJoinsSorts($query);
            }
        }

        if($this->hasCount()) {
            return response()->json(['count' => $query->count()], 200);
        }
//        $sql = str_replace_array('?', $query->getBindings(), $query->toSql()); dd($sql);
//        dd($sql);
        return $this->response->paginator($query->paginate($pageCount), $this->transformer, ['key' => $this->className]);
    }

    /**
     * Salva novo registro.
     *
     * @return \Dingo\Api\Http\Response
     */
    public function store(Request $request) {
        $entity = new $this->model($request->all());

        if(!$entity->save()) {
            return response()->json(['mensagem' => 'Os dados informados não foram validados', $entity->errors()], 400);
        }

        return $this->response->item($entity, $this->transformer, ['key' => $this->className]);
    }


    /**
     * Recupera registro pelo id.
     *
     * @return \Dingo\Api\Http\Response
     */
    public function show($id) {
        $entity = $this->model::find($id);

        if(!$entity) {
            return $this->notFound();
        }

        return $this->response->item($entity, $this->transformer, ['key' => $this->className]);
    }

    /**
     * Atualiza um registro pelo id.
     *
     * @return \Dingo\Api\Http\Response
     */
    public function update(Request $request, $id) {
        $entity = $this->model::find($id);

        if(!$entity) {
            return $this->notFound();
        }

        $entity->fill($request->all());

        if(!$entity->save()) {
            return response()->json(['mensagem' => 'Os dados informados não foram validados', $entity->errors()], 400);
        }

        return $this->response->item($entity, $this->transformer, ['key' => $this->className]);
    }


    /**
     * Remove um registro pelo id.
     *
     * @return \Dingo\Api\Http\Response
     */
    public function destroy($id) {
        $entity = $this->model::find($id);

        if(!$entity) {
            return $this->response->errorNotFound("Entidade não encontrada");
        }

        if(!$entity->delete()) {
            return response()->json(['mensagem' => 'Não foi possível excluir o registro', $entity->errors()], 406);
        }

        return $this->response->error("Registro excluido com sucesso", 200);
    }

    private function notFound() {
        return $this->response->errorNotFound("Entidade não encontrada");
    }

    private function hasCount() {
        return (key_exists('count', request()->query()) && request()->query()['count'] === 'true');
    }

    private function addJoinsSorts($query) {
        $allowedSorts = $this->apiDef::getAllowedSort();
        $joins = [];

        if (request()->get('sort')) {
            $sorts = explode(',', request()->get('sort'));
            foreach ($sorts as $sort) {
                if(Str::contains($sort, '.')) {
                    $include = str_replace('-', '', explode('.', $sort)[0]);
                    if(!in_array($include, $joins)) {
                        $model = new $this->model();
                        $relacao = $model->{$include}();
                        $query->leftJoin(
                            $relacao->getModel()->getTable(). " AS " .$include,
                            $include.".".$relacao->getOwnerKey(),
                            '=',
                            $model->getTable().".".$relacao->getForeignKey()
                        );
                        $joins[] = $include;
                    }
                }
            }
        }

        if($joins) {
            $query->select((new $this->model())->getTable().".*");
        }

        return $query;
    }
}
