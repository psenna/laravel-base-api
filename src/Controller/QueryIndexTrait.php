<?php

namespace Psenna\LaravelApi\Controller;

trait QueryIndexTrait {
      /**
     * Request to retrieve a collection of all items of this resource
     *
     * @return \Dingo\Api\Http\Response
     */
    public function getAll()
    {
        if (!(self::$publicEndpoints['getAll'] ?? false)) {
            $this->authorizeUserAction('viewAll');
        }

        $perPage = self::$defaultPageCount;

        if (request()->get('limit') && request()->get('limit') != 0) {
            $perPage = min(request()->get('limit'), $this->defaultPagelimit);
        }

        $query = QueryBuilder::for(self::$model);

        if(self::$apiDef) {
            // Adiciona filtros
            if(is_array(self::$apiDef::getAllowedFilters()))
            {
                $query->allowedFilters(self::$apiDef::getAllowedFilters());
            }

            if(is_array(self::$apiDef::getAllowedIncludes()) && !$this->hasCount())
            {
                $query->allowedIncludes(self::$apiDef::getAllowedIncludes());
            }

            if(is_array(self::$apiDef::getAllowedFields()) && !$this->hasCount())
            {
                $query->allowedFields(self::$apiDef::getAllowedFields());
            }

            if(is_array(self::$apiDef::getAllowedSort()) && !$this->hasCount())
            {
                $query->allowedSorts(self::$apiDef::getAllowedSort());
                $this->addJoinsSorts($query);
            }
        }

        if (!(self::$publicEndpoints['getAll'] ?? false)) {
            $this->qualifyCollectionQuery($query);
        }

        $paginator = $query->paginate($perPage);

        return $this->response->paginator($paginator, self::$transformer, ['key' => self::$className]);
    }
    
    private function notFound() {
        return $this->response->errorNotFound("Entidade não encontrada");
    }

    private function hasCount() {
        return (key_exists('count', request()->query()) && request()->query()['count'] === 'true');
    }

    private function addJoinsSorts($query) {
        $allowedSorts = $this->apiDef::getAllowedSort();
        $joins = [];

        if (request()->get('sort')) {
            $sorts = explode(',', request()->get('sort'));
            foreach ($sorts as $sort) {
                if(Str::contains($sort, '.')) {
                    $include = str_replace('-', '', explode('.', $sort)[0]);
                    if(!in_array($include, $joins)) {
                        $model = new $this->model();
                        $relacao = $model->{$include}();
                        $query->leftJoin(
                            $relacao->getModel()->getTable(). " AS " .$include,
                            $include.".".$relacao->getOwnerKey(),
                            '=',
                            $model->getTable().".".$relacao->getForeignKey()
                        );
                        $joins[] = $include;
                    }
                }
            }
        }

        if($joins) {
            $query->select((new $this->model())->getTable().".*");
        }

        return $query;
    }
}
