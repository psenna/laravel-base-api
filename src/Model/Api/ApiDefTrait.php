<?php

namespace Psenna\LaravelApi\Model\Api;

trait ApiDefTrait {
    public static function getAllowedFilters() {
        return null;
    }

    public static function getAllowedFields() {
        return null;
    }

    public static function getAllowedIncludes() {
        return null;
    }

    public static function getAllowedSort() {
        return null;
    }
}
