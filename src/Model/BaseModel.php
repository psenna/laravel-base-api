<?php


namespace Psenna\LaravelApi\Model;

use Closure;
use LaravelArdent\Ardent\Ardent;

abstract class BaseModel extends Ardent
{
    public function save(array $rules = array(),
                         array $customMessages = array(),
                         array $options = array(),
                         Closure $beforeSave = null,
                         Closure $afterSave = null) {
        if(!$rules && method_exists($this,'getRules')) {
            $rules = $this->getRules();
        }

        if(!$customMessages && method_exists($this,'getMessages')) {
            $customMessages = $this->getMessages();
        }

        return parent::save($rules, $customMessages, $options, $beforeSave, $afterSave);
    }
}
