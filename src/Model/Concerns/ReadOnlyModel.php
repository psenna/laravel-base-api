<?php

namespace Psenna\LaravelApi\Model\Concerns;

trait ReadOnlyModel {
    public function save(array $attributes = [], array $options = []){
        if(env('APP_ENV') == 'testing') {
            return parent::save($attributes, $options);
        }
        return false;
    }
    public function update(array $attributes = [], array $options = []){
        if(env('APP_ENV') == 'testing') {
            return parent::update($attributes, $options);
        }
        return false;
    }

    static function firstOrCreate(array $arr){
        return false;
    }

    static function firstOrNew(array $arr){
        return false;
    }

    public function delete(){
        if(env('APP_ENV') == 'testing') {
            return parent::delete();
        }
        return false;
    }

    static function destroy($ids){
        if(env('APP_ENV') == 'testing') {
            return parent::destroy($ids);
        }
        return false;
    }

    public function restore(){
        return false;
    }

    public function forceDelete(){
        return false;
    }

    /* We need to disable date mutators, because they brake toArray function on this model */
    public function getDates(){
        return array();
    }
}
