<?php

namespace Psenna\LaravelApi\Observers;

use App\Models\AplicacaoLog;
use Illuminate\Support\MessageBag;

class BaseRuleObserver
{
    protected function executaRegras($prefixo, $modelo)
    {
        if(!key_exists('validationErrors', $modelo)) {
            $modelo->validationErrors = new MessageBag();
        }

        $todosOsMetodos = get_class_methods($this);

        foreach($todosOsMetodos as $metodo) {
            $posicao = strpos($metodo, $prefixo);

            if ($posicao !== false && $posicao === 0) {
                $this->{$metodo}($modelo);
            }
        }

        if(!$modelo->validationErrors->isEmpty()) {
            return false;
        }

        return true;
    }

    protected function criarLog($original, $atual, $tipo){
        $log = new AplicacaoLog(['tipo' => $tipo, 'estado_anterior' => $original, 'estado_atual' => $atual, 'usuario' => request()->user->pessoa_id ?? 0]);
        $log->save();
    }
}
